//TODO File Header

// DiscordJS Package
const Discord = require('discord.js');

// File IO
const fs = require('fs');

// JS-Logger Package
const Logger = require('js-logger');

const ConfigHandler = require("./config/configurationHandler");

const prefix = "&&";

const GuildSubs = require('./model/guildSubs');
GuildSubs.initialize();

// The actual Discord User/Client
const client = new Discord.Client();

// Commands
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);

    client.commands.set(command.name, command);
}

// Logger Configuration
Logger.useDefaults();

ConfigHandler.read();

client.on('ready', () => {
    Logger.debug(`Logged in as ${client.user.tag}!`);

    client.user.setActivity(`Serving ${client.guilds.size} servers`);
});


// Executed when R3dC0rd joins a new guild
client.on("guildCreate", guild => {
    Logger.debug(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
    client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

// Executed when R3dC0rd is removed from a guild
client.on("guildDelete", guild => {
    Logger.debug(`I have been removed from: ${guild.name} (id: ${guild.id})`);
    client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

// Executed when R3dC0rd receives a new message
client.on('message', async msg => {

    if(msg.author.bot) return;

    if(msg.content.indexOf(prefix) !== 0) return;

    // Example Command: &&sub add soccer
    // ARGS: add soccer
    // COMMAND: sub
    const args = msg.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    try {
        client.commands.get(command).run(msg, args);
    }
    catch (error) {
        Logger.warn(error);
        msg.reply('there was an error trying to execute that command!');
    }
});



// Save subscriptions file each 5 minutes
setInterval(ConfigHandler.save, 60 * 1000);

// Logs Client
client.login(process.env.DISCORD_TOKEN)
    .then(Logger.debug)
    .catch(Logger.warn);