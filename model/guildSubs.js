var guildSubscriptions;

function initialize() {
    guildSubscriptions = {};
}

function subscribeToSub(guildID, newSub) {
    guildSubscriptions[guildID] =
        guildSubscriptions[guildID] || [];

    if (guildSubscriptions[guildID].includes(newSub)) {
        throw "is already subscribed to sub";
    }
    guildSubscriptions[guildID].push(newSub);
}

function removeSub(guildID, sub) {
    guildSubscriptions[guildID].splice(guildSubscriptions[guildID].indexOf(sub), 1);

    // If guild removed all subscriptions, remove guildID from guildSubscriptions
    if (guildSubscriptions.length == 0) {
        delete guildSubscriptions[guildID];
    }
}

module.exports = {
    getGuildSubs: function() {
        return guildSubscriptions;
    },
    initialize: initialize,
    setGuildSubs: function(data) {
      guildSubscriptions = data;
    },
    subscribeToSub: subscribeToSub,
    removeSub: removeSub
}