const Discord = require('discord.js');
const timeago = require("timeago.js");

class DiscordEmbed {

    constructor(title, author, subName, subURL, score, comments,
                created, postUrl, content, thumbnail) {
        this.title = limitCharacters(title, 256);
        this.author = author;
        this.subName = subName;
        this.subURL = subURL;
        this.score = score;
        this.comments = comments;
        this.created = processCreated(created);
        this.postUrl = postUrl;
        this.content = limitCharacters(content, 200);
        this.thumbnail = thumbnail;
    }

    getEmbed() {
        //Generates random colors for embed color
        let red = Math.random() * 256;
        let green = Math.random() * 256;
        let blue = Math.random() * 256;
        
        let discordEmbed = new Discord.RichEmbed()
            .setTitle(this.title)
            .setURL(this.postUrl)
            .setColor([red, green, blue])
            .setAuthor(this.subName, undefined, this.subURL)
            .setDescription(this.content)
            .addField("Score", this.score, true)
            .addField("Comments", this.comments, true)
            .setFooter(this.author + "  |  " + this.created, undefined);
        
        if (this.thumbnail !== "self") {
            discordEmbed.setThumbnail(this.thumbnail);
        }
        return discordEmbed;
    }
}

function limitCharacters(content, maxSize) {
    if (content.length > maxSize) {
        content = content.substring(0, maxSize - 3) + "...";
    }

    return content;
}

function processCreated(created) {
    let date = new Date(parseInt(created, 10) * 1000);

    return timeago.format(date);
}

module.exports = DiscordEmbed;