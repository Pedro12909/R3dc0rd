const DiscordEmbed = require('./DiscordEmbed');

function createPost(data) {
    // Check type of post
    let postType = checkPostType(data);

    let post = null;

    if (postType === 'text') {
        post = new DiscordEmbed(data.postTitle, data.postAuthor, data.subreddit,
            data.linkToSub, data.postScore, data.comments, data.created,
            data.linkToThread, data.postText, data.thumbnail);

        return post.getEmbed();
    }  else if(postType === 'link') {
        post = new DiscordEmbed(data.postTitle, data.postAuthor, data.subreddit,
            data.linkToSub, data.postScore, data.comments, data.created,
            data.linkToThread, data.postContentURL, data.thumbnail);

        return post.getEmbed();
    }
}

/**
 * Checks if a post is a text post (contains selfText) or an url
 * @param data post data
 * @returns {string}
 */
function checkPostType(data) {
    if (data.postText === "") {
        return 'link';
    }

    return 'text';
}

module.exports.createPost = createPost;