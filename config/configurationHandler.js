//TODO Header

// JS-Logger Package
const Logger = require('js-logger');

// File IO
const fs = require('fs');

// Object that holds all subscriptions for each guild
const GuildSubs = require('../model/guildSubs');

function saveSubscriptions() {
    Logger.debug("Started saving guild subscriptions to ./subscriptions.json");

    // Convert guild subscriptions object to JSON String
    let subscriptionsString = JSON.stringify(GuildSubs.getGuildSubs());

    Logger.debug(`About to write:\n${subscriptionsString}`);

    fs.writeFile('./subscriptions.json', subscriptionsString, (err) => {
        if (err) {
            Logger.warn("There was an error saving the subscriptions file");
        }
        Logger.debug("Subscriptions successfully saved to file.")
    })

}

function readSubscriptionFile() {
    fs.readFile('./subscriptions.json', (err, data) => {
        if (err) {
            Logger.warn("Could not find subscriptions file. Creating one...");
            return;
        }

        Logger.debug("Found subscriptions file! Loading contents...");

        GuildSubs.setGuildSubs(JSON.parse(data));
    })
}

module.exports = {
    save: saveSubscriptions,
    read: readSubscriptionFile
};