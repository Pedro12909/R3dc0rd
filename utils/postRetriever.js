// API Requests
const fetch = require("node-fetch");

function fetchPostData(url, numberOfPosts) {
    if (url === undefined) {
        throw 'Given undefined url'
    }

    if (isNaN(numberOfPosts)) {
        throw 'Number of posts to display must be a valid integer.'
    }

    if (numberOfPosts > 10) {
        throw "Can't display more than 10 posts at a time."
    } else if (numberOfPosts < 1) {
        throw 'At least one post should be displayed.'
    }

    var retrievedPosts = [];

    return fetch(url).then(response => {
        return response.json();
    }).then(data => {
        let posts = data.data.children;

        if (posts === undefined || posts.length === 0) {
            throw "There was an error fetching posts from that subreddit";
        } else {
            for (let i = 0; i < numberOfPosts; i++) {
                let currentPost = posts[i].data;

                let postData = {
                    postTitle: currentPost.title,
                    postScore: currentPost.score,
                    thumbnail: currentPost.thumbnail,
                    postHint: currentPost.post_hint,
                    postContentURL: currentPost.url,
                    linkToSub: "https://reddit.com/r/" + currentPost.subreddit,
                    linkToThread: "https://reddit.com" + currentPost.permalink,
                    postAuthor: currentPost.author,
                    isNSFW: currentPost.over_18,
                    created: currentPost.created_utc,
                    postText: currentPost.selftext,
                    subreddit: "/r/" + currentPost.subreddit,
                    comments: currentPost.num_comments
                };

                retrievedPosts.push(postData);
            }

            return retrievedPosts;
        }
    }).catch(err => {
        throw err;
    });
}

module.exports = {
    fetchPostData
};