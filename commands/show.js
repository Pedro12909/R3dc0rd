// JS-Logger Package
const Logger = require('js-logger');

const PostRetriever = require('../utils/postRetriever');
const PostFactory = require('../model/EmbedFactory');

const DEFAULT_POST_AMOUNT = 5;

function run(message, args) {
    if (args === undefined) {
        Logger.warn("undefined args given.");
        message.reply("There was an error processing your command!");
    }

    if (args.length === 0) {
        Logger.warn("Tried to use show command with no arguments.");
        message.reply("Invalid command syntax.");
    }

    // Get URL/query from args
    let url = queryParser(args);

    // Use PostRetriever to fetch data from the sub
    PostRetriever.fetchPostData(url, DEFAULT_POST_AMOUNT)
        .then(posts => {
            for (let i = 0; i < posts.length; i++) {
                try {
                    // Create embeds with formatted data
                    let embed = PostFactory.createPost(posts[i]);

                    // Display data
                    message.channel.send({embed});
                } catch (err) {
                    message.reply("There was an error processing your request");
                    Logger.warn(err);
                }
            }
        })
        .catch(err => {
            message.reply("There as an error with your request.");
            Logger.warn(err);
        });
}

function queryParser(args) {
    const BASE_URL = "https://reddit.com/r/";
    const SUFFIX = ".json";
    const SUB_NAME = args[0];

    let result = BASE_URL + SUB_NAME;

    if (args.length === 1) {
        return result += SUFFIX;
    }

    let sortTypes = ["hot", "new", "controversial", "top", "rising"];
    let timePeriods = ["hour", "day", "week", "month", "year", "all"];

    let chosenSort = "";
    let chosenTimePeriod = "";
    for (let i = 1; i < args.length; i++) {
        const currentArg = args[i].toLowerCase();
        if (sortTypes.includes(currentArg)) {
            chosenSort = '/' + currentArg;
        }

        if (timePeriods.includes(currentArg)) {
            chosenTimePeriod = '/?t=' + currentArg;
        }
    }

    result += chosenSort;
    
    if (chosenSort === "/top" || chosenSort === "/controversial") {
        result += chosenTimePeriod;
    }

    result += SUFFIX;

    return result;
}


module.exports = {
    name: "show",
    queryParser,
    run
};