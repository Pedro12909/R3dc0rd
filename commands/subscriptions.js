// API Requests
const fetch = require("node-fetch");

const GuildSubs = require('../model/guildSubs');

// JS-Logger Package
const Logger = require('js-logger');

function run(message, args) {
    if (args[0] === "list") {
        let guildID = message.guild.id;
        let guildName = message.guild.name;
        let subs = GuildSubs.getGuildSubs()[guildID];

        if (subs) {
            let response = `Showing subscriptions for ${guildName}:`;

            Logger.debug(subs);
            let counter = 0;

            subs.forEach((sub) => {
                response += `\n${counter} - /r/${sub}`;
                counter++;
            });
            message.reply(response);
        } else {
            message.reply(`No active subscriptions for ${guildName}.`);
        }
    } else if (args[0] === "add") {
        let newSub = args[1];

        let url = "https://www.reddit.com/r/" + newSub + ".json";

        fetch(url).then(response => {
            return response.json();
        }).then(data => {
            let posts = data.data.children;

            if (posts === undefined || posts.length == 0) {
                Logger.warn("Subreddit doesn't exist.");
                message.reply("Subreddit doesn't exist.");
            } else {
                try {
                    GuildSubs.subscribeToSub(message.guild.id, newSub);
                } catch (error) {
                    Logger.debug(message.guild.name + " " + error);
                    message.reply(message.guild.name + " " + error);
                    return;
                }

                Logger.debug(`${message.guild.name} just subscribed to /r/${newSub}!`);
                message.reply(`${message.guild.name} just subscribed to /r/${newSub}!`);
            }
        }).catch(err => {
            Logger.warn(err);
        });
    } else if (args[0] === "remove") {
        let sub = args[1];

        let guildSubs = GuildSubs.getGuildSubs()[message.guild.id];

        let indexOfSub = guildSubs.indexOf(sub);

        if (indexOfSub == -1) {
            message.reply(`You're not subscribed to ${sub}!`);
            return;
        }

        Logger.debug("Found sub to remove. Removing...");
        GuildSubs.removeSub(message.guild.id, sub);

        message.reply(`Successfully unsubscribed from ${sub}!`);
        Logger.debug(`${message.guild.name} unsubscribed from ${sub}!`);
    }
}

module.exports = {
    name: "sub",
    run
}