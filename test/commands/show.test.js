var show    = require('../../commands/show'),
    assert  = require('chai').assert;

describe("Show Command Test Suite", function() {

    describe("Query Parser", function() {
        const BASE_URL = "https://reddit.com/r/";
        const SUFFIX = ".json";

        it('default args/input', function () {
            let args = ["sub"];

            let expected = BASE_URL + "sub" + SUFFIX;
            let actual = show.queryParser(args);

            assert.equal(actual, expected);
        });

        describe("sort without time period", function() {
            it('sort by new', function () {
                let args = ["sub", "new"];

                let expected = BASE_URL + "sub/new" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by hot', function () {
                let args = ["sub", "hot"];

                let expected = BASE_URL + "sub/hot" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by controversial', function () {
                let args = ["sub", "controversial"];

                let expected = BASE_URL + "sub/controversial" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by top', function () {
                let args = ["sub", "top"];

                let expected = BASE_URL + "sub/top" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by rising', function () {
                let args = ["sub", "rising"];

                let expected = BASE_URL + "sub/rising" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });
        });

        describe('sort with time periods', function () {
            it('sort by top last hour', function () {
                let args = ["sub", "top", "hour"];

                let expected = BASE_URL + "sub/top/?t=hour" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by top last 24 hours', function () {
                let args = ["sub", "top", "day"];

                let expected = BASE_URL + "sub/top/?t=day" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by top last week', function () {
                let args = ["sub", "top", "week"];

                let expected = BASE_URL + "sub/top/?t=week" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by top last month', function () {
                let args = ["sub", "top", "month"];

                let expected = BASE_URL + "sub/top/?t=month" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by top last year', function () {
                let args = ["sub", "top", "year"];

                let expected = BASE_URL + "sub/top/?t=year" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('sort by top of all time', function () {
                let args = ["sub", "top", "all"];

                let expected = BASE_URL + "sub/top/?t=all" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });
        });

        describe('sort with time periods and other spam', function () {
            it('reversed order of sort and time period', function () {
                let args = ["sub", "all", "top", "spam", "123"];

                let expected = BASE_URL + "sub/top/?t=all" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });

            it('spam in between sort type and time period', function () {
                let args = ["sub", "all", "test", "top", "spam"];

                let expected = BASE_URL + "sub/top/?t=all" + SUFFIX;
                let actual = show.queryParser(args);

                assert.equal(actual, expected);
            });
        });
    });
});