# R3dC0rd [![Build Status](https://travis-ci.org/Pedro12909/R3dc0rd.svg?branch=master)](https://travis-ci.org/Pedro12909/R3dc0rd)

A Discord Bot that fetches a daily/weekly digest of trending posts from Reddit.
